Attribute VB_Name = "modWordApplication"
Option Explicit

'-----------------------------------------------------------------
'Author : John Henrik Andersen / NetCenter
'Date   : 1998.08.04
'
'Description:
'   tries to aquire a link to an existing Word application.
'   if none is found, it creates a new instance of Word.
'-----------------------------------------------------------------
Public Function gGetWordApplication() As Word.Application
    Dim wdApp As Word.Application
    On Error Resume Next
    Set wdApp = GetObject(, "Word.Application")
    'Set wdApp = CreateObject("Word.Application")
    If Err.Number <> 0 Then
        Set wdApp = New Word.Application
    End If
    On Error GoTo 0
    Set gGetWordApplication = wdApp
End Function

