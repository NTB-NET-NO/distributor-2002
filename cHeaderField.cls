VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cHeaderField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Name:          cHeaderField
'Date:          2000.02.01
'Author:        Jens Erik Taasen
'Description:   Class holding Fingerpost headers for the
'               output file with corresponding fields
'               in the Exchange form.

Option Explicit

'local vars
Private mvarFipHeader As String
Private mvarFormField As String
Private mvarDefaultValue As String

Public Property Let FipHeader(ByVal vData As String)
    mvarFipHeader = vData
End Property

Public Property Get FipHeader() As String
    FipHeader = mvarFipHeader
End Property

Public Property Let FormField(ByVal vData As String)
    mvarFormField = vData
End Property

Public Property Get FormField() As String
    FormField = mvarFormField
End Property

Public Property Let DefaultValue(ByVal vData As String)
    mvarDefaultValue = vData
End Property

Public Property Get DefaultValue() As String
    DefaultValue = mvarDefaultValue
End Property

