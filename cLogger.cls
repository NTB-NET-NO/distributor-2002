VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cLogger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'Name:          cLogger
'Date:          99.11.24
'Author:        Svein Terje Gaup
'Description:   Class to write data to a log file.
'Properties:    FileName, Message, Source

Option Explicit

'local variable(s) to hold property value(s)
Private mvarFilename As String 'local copy
Private mvarMessage As String 'local copy
Private mvarSource As String 'local copy

Public Sub Log(Optional strFileName As String, Optional strSource As String, Optional strMessage As String)
Attribute Log.VB_UserMemId = 0
'Date:          99.11.24
'Author:        Svein Terje Gaup
'Description:   This is the main method of this class.
'               It writes data to a log-file.

    Dim filehandle As Integer
    
    'If any of the optional parameters are set,
    'change value of properties accordingly.
    If strFileName <> "" Then Me.FileName = strFileName
    If strSource <> "" Then Me.Source = strSource
    If strMessage <> "" Then Me.Message = strMessage
    
    'Get an unused file handle
    filehandle = FreeFile

    On Error Resume Next
    
    'If no filename is specified, set it to a default value
    If mvarFilename = "" Then mvarFilename = "distributor.log"
    
    'Open file for appending
    Open mvarFilename For Append As #filehandle
    
    'If file is not found then create a new one
    If Err.Number <> 0 Then
        Open mvarFilename For Output As #filehandle
    End If
    
    On Error GoTo 0
    
    'Write data to log-file, then close file
    Print #filehandle, Now() & " " & mvarSource & " : " & mvarMessage
    Close #filehandle
        
End Sub

Public Sub Init(strFileName As String, strSource As String, strMessage)

End Sub

Public Property Let Source(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Source = 5
    mvarSource = vData
End Property

Public Property Get Source() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Source
    Source = mvarSource
End Property

Public Property Let Message(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Message = 5
    mvarMessage = vData
End Property

Public Property Get Message() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Message
    Message = mvarMessage
End Property

Public Property Let FileName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Filename = 5
    mvarFilename = vData
End Property

Public Property Get FileName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Filename
    FileName = mvarFilename
End Property

