Attribute VB_Name = "ModINIFileHandling"
'Date:          1999.05.21
'Description:   This module declares function(s) to read values from an ini file.
'Author:        Nicked by Svein Terje Gaup from the Gudrun project (John Henrik Andersen).

Option Explicit

'Library functions from kernel32
Public Declare Function gfnGetPrivateProfileString Lib "KERNEL32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function gfnGetPrivateProfileInt Lib "KERNEL32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Function gfnGetFromIni(iniFile As String, strSection As String, strKey As String, strDefaultValue As String) As String

'Date:          1999.05.21
'Description:   This function is a wrapper for the "GetPrivateProfileStringA" function.
'Author:        John Henrik Andersen, minor modifications by Svein Terje Gaup
'Input:         Section of ini file, Key in ini file, Default value if key is not found or key value is empty
'Output:        Value from ini file
'Uses:          GetPrivateProfileStringA from kernel32

    Dim intI As Long
    Dim strReturnString As String   'String buffer
    
    'Fills string buffer with null-character (Ascii char number 0)
    strReturnString = String$(150, Chr$(0))

    'Gets value from ini file
    intI = gfnGetPrivateProfileString(strSection, strKey, strDefaultValue, strReturnString, Len(strReturnString) + 1, iniFile)
    gfnGetFromIni = Mid(strReturnString, 1, intI)
    ' Removed old obsolete code below AXIS/PE 15.6.2001
    'Removes null-characters from end of string
'    For intI = 0 To Len(strReturnString) + 1
'        If Asc(Right(Left(strReturnString, intI + 1), 1)) = 0 Then
'            Exit For
'        End If
'    Next

    'Returns String without null characters
 '   gfnGetFromIni = Left(strReturnString, intI)

End Function
