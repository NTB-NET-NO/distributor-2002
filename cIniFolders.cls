VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cIniFolders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private strFolder As String
Private strOutDir As String
Private strCopies As String
Private intDelay As Integer

'Folder to watch
Public Property Get Folder() As String
    Folder = strFolder
End Property

Public Property Let Folder(ByVal strNewValue As String)
    strFolder = strNewValue
End Property

'Alternative output directory
Public Property Get OutDir() As String
    OutDir = strOutDir
End Property

Public Property Let OutDir(ByVal strNewValue As String)
    strOutDir = strNewValue
End Property

'Copy folders (separated by ";")
Public Property Get CopyFolders() As String
    CopyFolders = strCopies
End Property

Public Property Let CopyFolders(ByVal strNewValue As String)
    strCopies = strNewValue
End Property

'Delay
Public Property Get Delay() As Integer
    Delay = intDelay
End Property

Public Property Let Delay(ByVal intNewValue As Integer)
    intDelay = intNewValue
End Property
