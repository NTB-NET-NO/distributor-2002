VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cMsgFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private objFolder As MAPI.Folder
Private strFolderName As String
Private strOutDir As String
Private intDelay As Integer
Private datLastExport As Date
Private colCopyFolders As New Collection

'Base name of the path
Public Property Let FolderName(ByVal strNewValue As String)
    strFolderName = strNewValue
End Property

Public Property Get FolderName() As String
    FolderName = strFolderName
End Property

'Pointer to the MAPI folder
Public Property Set Folder(ByVal objNewValue As MAPI.Folder)
    Set objFolder = objNewValue
End Property

Public Property Get Folder() As MAPI.Folder
    Set Folder = objFolder
End Property

'Pointer to collection of MAPI folders (for copies)
Public Property Set CopyFolders(ByVal colNewValue As Collection)
    Set colCopyFolders = colNewValue
End Property

Public Property Get CopyFolders() As Collection
    Set CopyFolders = colCopyFolders
End Property

'Alternative out dir
Public Property Get OutDir() As String
    OutDir = strOutDir
End Property

Public Property Let OutDir(ByVal strNewValue As String)
    strOutDir = strNewValue
End Property

'Delay (secs between each export)
Public Property Get Delay() As Integer
    Delay = intDelay
End Property

Public Property Let Delay(ByVal intNewValue As Integer)
    intDelay = intNewValue
End Property

'Time of the last export
Public Property Get LastExport() As Date
    LastExport = datLastExport
End Property

Public Property Let LastExport(ByVal datNewValue As Date)
    datLastExport = datNewValue
End Property
