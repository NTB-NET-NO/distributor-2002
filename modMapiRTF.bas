Attribute VB_Name = "modMapiRTF"
Public Declare Function ReadRTF Lib "mapirtf.dll" Alias "readrtf" _
                            (ByVal ProfileName As String, _
                            ByVal SrcMsgID As String, _
                            ByVal StoreID As String, _
                            ByRef MsgRTF As String, _
                            ByRef bytesRead As Long) As Integer

Public Declare Function WriteRTF Lib "mapirtf.dll" Alias "writertf" _
                            (ByVal ProfileName As String, _
                            ByVal MessageID As String, _
                            ByVal StoreID As String, _
                            ByVal cText As String) As Integer


